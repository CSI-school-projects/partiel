import { StatusBar } from 'expo-status-bar';
import { 
  StyleSheet, 
  Text, 
  View, 
  Image,
  Button,
  TextInput,
  FlatList,
} from 'react-native';
import React, { useState, useEffect } from 'react';

export default function App() {
  const [data, setData] = useState(null);
  const [data2, setData2] = useState(null);
  const [num, setNum] = useState(0);
  const [list, setList] = useState(data2);

  const getQuote = async () => {
    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?`)
        .then(response => response.json())
        .then(data => {
          setData(data);
        });
  };

  const getQuoteByNumber = async () => {
    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${num}`)
        .then(response => response.json())
        .then(data => {
          setData2(data);
          console.log('try');
          console.log(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${num}`);
          console.log(num);
          console.log(data2);
        });
  };

  // const renderItem = ({ item }) => (
  //   <View style={styles.liste} key={item}>
  //     <Image style={styles.image} source={{
  //       uri: `${item.image}`
  //     }}/>
  //     <Text>Character is : {item.character}</Text>
  //     <Text>Quote is : {item.quote}</Text>
  //   </View>
  // );

  // const flatList = () => { 
  //   return <FlatList
  //     data={data2}
  //     renderItem = {renderItem}
  //     keyExtractor = {item => item.id }
  //   />
  // }

  // const showButton = () => {
  //   setList(
  //     [...list, {}]
  //   );
  // }

  const Separator = () => ( <View style={styles.separator} /> );

  useEffect(() => {
    getQuote();
  }, []);

  useEffect(() => {
    getQuoteByNumber();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Partiel React Native</Text>
      <Separator/>
      <View>
        {data && 
        (
          <>
            <Text>Step - #1</Text>
            <Separator/>
              <Image style={styles.image} source={{
                uri: `${data[0].image}`
              }}/>
              <Text>Character is : {data[0].character}</Text>
              <Text>Quote is : {data[0].quote}</Text>
            <Button
              style={styles.button}
              title="Reload"
              onPress={() => getQuote()}
            />
          </>
        )}
        <Separator/>
        {data2 &&
        (
          <>
            <Text>Step - #2</Text>
            <Separator/>
            <Text>Number of quote wanted :</Text>
            <TextInput
              style={styles.input}
              value = { num }
              placeholderTextColor='#FFFFFF' 
              onChangeText={(input) => setNum(input)}
            />
            <Button
              style={styles.button}
              title="Show"
              onPress={getQuoteByNumber()}
            />
          </>
        )
        }
        <StatusBar style="auto" />
      </View>
      {/* 
        <Separator/>
        <Button
          title="Add"
          underlayColor="white"
          style={styles.addButton}
          onPress={onPressAddButton}
        /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
    width: 50,
    height: 100,
  },
  liste: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  separator: {
    marginVertical: 8,
    marginHorizontal: 15,
    borderBottomColor: '#0000FF',
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignSelf:'stretch'
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    textAlign: 'center',
    marginRight: 30,
    width: 70,
  },
  input: {
    borderColor: '#0066cc',
    color: '#00000',
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
